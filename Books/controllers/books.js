'use strict';
var views = require('co-views');
var parse = require('co-body');
var wrap = require('co-monk');
var monk = require('monk');
var db = monk('localhost:27017/library');
var co = require('co');
var books = db.get('books');

co(function * () {
  var books = yield books.find({});
});
var render = views(__dirname + '/../views', {
  map: {
    html: 'swig'
  }
});

//html
module.exports.home = function* home(next) {
    if ('GET' != this.method) return yield next;
    this.body = yield render('layout');
};

module.exports.list = function* list(next) {
    if ('GET' != this.method) return yield next;
    this.body = yield render('list', {
        'books': yield books.find({})
    });
};

module.exports.bookById = function* bookById(id,next) {
    if ('GET' != this.method) return yield next;
    if (id === "" + parseInt(id, 10)) {
      var book = yield books.find({}, {
          'skip': id ,
          'limit': 1
      });
      if (!book) this.throw(404, 'Book is not found');
      this.body = yield render('bookShow', { book: book });  
    }
};

module.exports.bookByAuthorView = function* bookByAuthorView(authorName, next) {
    var book = yield books.find({ "author": authorName });
    if (book.length === 0) {
        this.throw(404, 'Books are not found');
    }
    this.body = yield render('booksOfOneAuthor', { book: book });
};

//api
module.exports.all = function * all(next) {
  if ('GET' != this.method) return yield next;
  this.body = yield books.find({});
};

module.exports.fetch = function * fetch(id,next) {
  if ('GET' != this.method) return yield next;
  if(id === ""+parseInt(id, 10)){
    var book = yield books.find({}, {
      'skip': id - 1,
      'limit': 1
    });
    if (book.length === 0) {
      this.throw(404, 'Book is not found');
    }
    this.body = yield book;
  }

};

module.exports.bookByAuthor = function* bookByAuthor(authorName, next){
    var book = yield books.find({"author": authorName});
    if (book.length === 0) {
        this.throw(404, 'Books are not found');
    }
    this.body = yield book;
};

module.exports.add = function * add(data,next) {
  if ('POST' != this.method) return yield next;
  var book = yield parse(this, {
    limit: '1kb'
  });
  var inserted = yield books.insert(book);
  if (!inserted) {
    this.throw(405, "Add error");
  }
  this.body = 'Error!';
};

module.exports.modify = function * modify(id,next) {
  if ('PUT' != this.method) return yield next;

  var data = yield parse(this, {
    limit: '1kb'
  });

  var book = yield books.find({}, {
    'skip': id - 1,
    'limit': 1
  });

  if (book.length === 0) {
    this.throw(404, 'Invalid id!');
  }

  var updated = books.update(book[0], {
    $set: data
  });

  if (!updated) {
    this.throw(404, "Update error!");
  } else {
    this.body = "Done";
  }
};

module.exports.remove = function * remove(id,next) {
  if ('DELETE' != this.method) return yield next;
  var book = yield books.find({}, {
    'skip': id - 1,
    'limit': 1
  });
  if (book.length === 0) {
    this.throw(404, 'Invalid id!');
  }
  var removed = books.remove(book[0]);
  if (!removed) {
    this.throw(404, "error!");
  } else {
    this.body = "Done";
  }

};
module.exports.head = function *(){
  return;
};
module.exports.options = function *() {
  this.body = "Allow: HEAD,GET,PUT,DELETE,OPTIONS";
};

