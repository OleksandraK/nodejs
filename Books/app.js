'use strict';
var books = require('./controllers/books');
var logger = require('koa-logger');
var serve = require('koa-static');
var route = require('koa-route');
var koa = require('koa');
var path = require('path');
var logger = require('koa-logger');
var app = module.exports = koa();

app.use(logger());


app.use(route.get('/', books.home));

//html
app.use(route.get('/view/blabla/:authorName', books.bookByAuthorView));
app.use(route.get('/view/books/', books.list));
app.use(route.get('/view/bookById/:id', books.bookById));


//api
app.use(route.get('/books/byAuthor/:authorName', books.bookByAuthor))
app.use(route.get('/books/:id', books.fetch));
app.use(route.get('/books/', books.all));
app.use(route.post('/books/', books.add));
app.use(route.put('/books/:id', books.modify));
app.use(route.delete('/books/:id', books.remove));

app.use(route.options('/', books.options));
app.use(route.trace('/', books.trace));
app.use(route.head('/', books.head));

app.use(serve(path.join(__dirname, 'public')));


if (!module.parent) {
  app.listen(1337);
  console.log('listening on port 1337');
}
